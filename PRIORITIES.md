# Priorities

## Direct report

## Quarterly Priorities

## Weekly Priorities

- [ ] Incremental repo backups opportunity canvas
- [ ] Communicating problems and solutions
- [ ] YAML fmt
- [ ] features.yml review
- [ ] OKR Pajamas triage
- [ ] Kick Off test

### 2021-01-04

- [x] Gitaly sales/support enablement deck
- [x] Gitaly direction review
- [ ] Schedule quarterly iteration retrospectives
- [ ] Schedule monthly stage PM/EM/UX sync

### 2020-11-02

- [ ] [Release post generator: creating merge requests for closed epics](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/66837)
- [ ] Release post generator: images problems
- [ ] Release post generator: feedback link

### 2020-08-24

- [ ] Create stage strategy
- [ ] [File locking docs]()
- [ ] Opportunity canvas
- [ ] [Board updates](https://gitlab.com/gitlab-com/Product/-/issues/1472)
- [ ] CDF reviews

### 2020-08-10

- [x] Finalize Gitaly 13.4 planning/handover
- [x] Create stage features.yml review
- [x] Investment/team size review
- [ ] [Create stage strategy](https://gitlab.com/gitlab-org/create-stage/-/issues/12704)
- [ ] Updated Create:Code Review and Create:Web IDE walk through video
- [ ] Binary file locking video and blog

### 2020-08-03

- [x] [Gitaly 13.4 planning](https://gitlab.com/gitlab-org/create-stage/-/issues/12708)
- [x] [Gitaly direction update](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/58570)
- [x] [Gitaly release post items](https://gitlab.com/dashboard/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Agitaly&label_name[]=release%20post&milestone_title=13.3)
  - [x] [Quorum voting](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/56578)
  - [x] [Reference transactions](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/57058)
  - [x] [Concurrent backups](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/57055)
  - [x] [Read distribution](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/58694)
  - [x] [Git 2.28 upgrade](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/58699)
- [x] Review Web IDE updates
- [x] [Performance Indicator update](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/58846)

### 2020-07-27

- [x] [Dev Group Converstation](https://docs.google.com/presentation/d/10Kv7WuAk8xuKdY_e2rFK6UnRIUXgkH6y_ts6llcnOF4/edit#slide=id.g156008d958_0_18)

### 2020-07-20

- [x] Release post improvements
- [x] Watch kick off videos
- [x] Monthly call with Daniel and SET (Mark L)
- [x] Performance indicators - add GMAU for each group, new fields

### 2020-07-13

- [x] [Code Review strategy review](https://about.gitlab.com/direction/create/code_review/)
- [ ] [Create stage strategy](https://gitlab.com/gitlab-org/create-stage/-/issues/12704)
- [ ] Updated Create:Code Review walk through video
- [ ] Gitaly sharding vs cluster
- [x] Gitaly 13.3 kick off

### 2020-07-06

- [x] [Release post JSON Schema](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/55168)
- [x] Release post items
  - [x] Praefect TLS
  - [x] Strong consistency
- [x] Gitaly monthly call and 13.3 planning
- [x] [CDFreview](https://about.gitlab.com/handbook/product/product-manager-role/)
- [x] [Create pricing strategy](https://gitlab.com/gitlab-com/Product/-/issues/1310)
- [x] [Web IDE category strategy review](https://about.gitlab.com/direction/create/web_ide/)

### 2020-06-29

- [x] [Create performance indicators](https://docs.google.com/presentation/d/1wGMDsUV14AdjDmRZ0sRpou-9MS7mYSSP2uf2YoWLGE8/edit]
- [x] [Consumption pricing - repo size reduction](https://www.youtube.com/watch?v=n6gwWURI9_Q)
- [x] [Gitaly speed run: partial clone and file locking](https://www.youtube.com/watch?v=LMG_-uJVdsw)
- [x] [Revise file locking docs](https://gitlab.com/gitlab-org/gitlab/-/issues/219957)

### 2020-06-22

- [x] [Git default branch](https://gitlab.com/gitlab-org/gitlab/-/issues/221164)
- [ ] [Revise file locking docs](https://gitlab.com/gitlab-org/gitlab/-/issues/219957)

### 2020-06-15

- [x] 360 reviews
- [ ] [Git default branch](https://gitlab.com/gitlab-org/gitlab/-/issues/221164)
- [x] [Gitaly direction update](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/50442)
- [x] Catching up

### 2020-06-08

Vacation

### 2020-05-25

- [x] Review direction for Source Code and Editor groups

### 2020-05-11

- [x] [User interviews template/process](https://gitlab.com/gitlab-com/Product/-/issues/1152)
- [x] Review [Reviewer assignment opportunity canvas](https://docs.google.com/document/d/1sl7cHqU_hASh2HAOX_jwfkkPb6_rGdGqyKoz1oXe778/edit)
- [x] Rename Gitaly HA
- [x] Release post items

### 2020-05-04

- [x] [Create stage iteration retrospective](https://gitlab.com/gitlab-org/create-stage/-/issues/12675)
- [x] Prepare for [Dev Section Group Conversation](https://gitlab.com/gitlab-com/Product/-/issues/11370)
- [x] [Gitaly North Star follow ups](https://gitlab.com/gitlab-org/create-stage/-/issues/12658)
- [x] Complete [Gitaly direction updates](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/48418)
- [x] Complete [Gitaly 13.1 planning](https://gitlab.com/gitlab-org/create-stage/-/issues/12655)
