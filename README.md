# README

At GitLab, I am the Group Product Manage, Create.

- [GitLab](https://gitlab.com/jramsay)
- [GitLab Team Page](https://about.gitlab.com/company/team/#jramsay)
- [Personal Feedback Form](https://docs.google.com/forms/d/e/1FAIpQLSc9DC1CR3jtR1t4rBEhmj-rnPIr0B0zuIZyZQMRQnasfa3xbA/viewform)

## Feedback

I am always appreciative of feedback, constructive criticism, or suggested improvements. I will ALWAYS thank you for it.

You can provide me direct (anonymous) feedback by filling out my [personal feedback form](https://docs.google.com/forms/d/e/1FAIpQLSc9DC1CR3jtR1t4rBEhmj-rnPIr0B0zuIZyZQMRQnasfa3xbA/viewform).
